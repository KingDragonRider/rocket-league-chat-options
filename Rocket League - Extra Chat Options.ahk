﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#Include Libraries/Jxon.ahk
#Include Libraries/XInput.ahk
#Include Libraries/Google Translate.ahk
#Include Libraries/Socket.ahk

I_Icon = Assets/Icons/icon.ico
ICON [I_Icon]                        ;Changes a compiled script's icon (.exe)
if I_Icon <>
IfExist, %I_Icon%
	Menu, Tray, Icon, %I_Icon%   ;Changes menu tray icon 

XInput_Init()

Thread, interrupt, 0
SetTimer, ControllerCheck, 5
SetTimer, MessageCheck, 50

#SingleInstance Force

global GuiWidth := 390
global GuiHeight := 368

global LowCharSpeed := 5
global HighCharSpeed := 10
global ChatOpenMillis := 10000

global LastButton := -1
global LastPressedMillis := 0
						
global ChatOptions := EnglishChatOptions

global ChatOptionCycles := []

global OutstandingMessages := []

global ButtonsPressed := []

global LanguagesMenuOpen := False
global SecondaryMessages := False
global EndGameMessages := False
global ForceNoMessageControlMillis := -1
global ChatModeSetting := 0
global PreviousChatMode := 1
global PreviewCycle := 0
global cancelMillis := 0

FileRead, JsonString, Assets/ChatOptions.json
if ErrorLevel
{
	println("Cannot find ChatOptions.json.")
	ExitApp
}
global jsonText := Jxon_Load(JsonString)

global translations := {}

global translatedOptionsFileName := "bin/TranslatedOptions.json"

FileRead, JsonString, %translatedOptionsFileName%
if not ErrorLevel
{
	translations := Jxon_Load(JsonString)
}

global translate := "en"
global languages := []
global languageCount := 0
global lastTranslate := ""

Gui, LanguagesGui: Font, s24
Gui, LanguagesGui: Add, Text, cWhite x0 y29 w390 h40 +Center, Languages
Gui, LanguagesGui: Add, Picture, % "x16 y" . (GuiHeight + 118) . " w24 h24" , Assets/BackButton24.png
Gui, LanguagesGui: Font, s10
Gui, LanguagesGui: Add, Text, % "cGray x56 y" . (GuiHeight + 125) . " w280 h20", Press the back button to toggle the chat mode

SetupLanguageGui()

Gui, ChatMode: Add, Picture, x4 y4 w37 h37 vChatModeIcon, Assets/TeamChat.png

Gui, Chat: Add, Picture, x16 y209 w40 h40 , Assets/LeftArrow.png
Gui, Chat: Add, Picture, x16 y89 w40 h40 , Assets/UpArrow.png
Gui, Chat: Add, Picture, x16 y149 w40 h40 , Assets/RightArrow.png
Gui, Chat: Add, Picture, x16 y269 w40 h40 , Assets/DownArrow.png
Gui, Chat: Add, Picture, % "x16 y" . (GuiHeight - 32) . " w24 h24" , Assets/BackButton24.png
Gui, Chat: Font, s10
Gui, Chat: Add, Text, % "cGray x56 y" . (GuiHeight - 28) . " w280 h20", Press the back button to cycle through phrases
Gui, Chat: Add, Text, % "cGray x" . (GuiWidth - 50) . " y8 w50 h60 vTimeLeft", 0s
Gui, Chat: Font, s24
Gui, Chat: Add, Text, cWhite x0 y29 w390 h40 +Center, Quick Chat 2.0
Gui, Chat: Font, s13
Gui, Chat: Add, Text, cWhite x75 y97 w280 h60 vUp,
Gui, Chat: Add, Text, cWhite x75 y157 w280 h60 vRight,
Gui, Chat: Add, Text, cWhite x75 y217 w280 h60 vLeft,
Gui, Chat: Add, Text, cWhite x75 y277 w280 h60 vDown,

InitialiseChat(true)

Server := new SocketTCP()
Server.OnAccept := Func("OnAccept")
Server.Bind(["127.0.0.1", 1338])
Server.Listen()

global LastCallForMessage := []
MinimumTicksBetweenCalls := 100

return


; --- Server Code, modified from https://gist.github.com/G33kDude/c94ce4a847ac59913ccf52b0a3075f61 ---

OnAccept(server)
{
	global
	
	sock := server.Accept()
	sockLine := sock.RecvLine()
	
	IsDuplicate := LastCallForMessage[sockLine] != "" && (A_TickCount - MinimumTicksBetweenCalls) < LastCallForMessage[sockLine]
	
	; to fix the websocket being called multiple times for a single button press (Quirk with Elgato stream deck software)
	if (IsDuplicate)
	{
		sock.SendText("HTTP/1.0 200 Too Many Requests`r`n`r`n")
		println("HTTP/1.0 200 Too Many Requests`r`n`r`n")
		sock.Disconnect()
		return
	}
	
	LastCallForMessage[sockLine] := A_TickCount
	
	println(sockLine)
	
	request := StrSplit(sockLine, " ")
	
	if (request[1] != "GET")
	{
		sock.SendText("HTTP/1.0 501 Not Implemented`r`n`r`n")
		sock.Disconnect()
		return
	}
	
	fullFunction := StrSplit(LTrim(request[2], "/"), ",")
	fname := fullFunction[1]
	arg := fullFunction[2]
	
	if IsFunc(fname)
	{
		if(arg != "") {
			customFunc := Func(fname).Bind(arg)
			SetTimer, % customFunc, -0
		}
		else {
			SetTimer, % fname, -0
		}
		sock.SendText("HTTP/1.0 200 OK`r`n`r`n")
		sock.Disconnect()
		return
	}
	
	sock.SendText("HTTP/1.0 404 Not Found`r`n`r`n")
	sock.Disconnect()
	return
}


MessageCheck:
	if(OutstandingMessages.Length() > 0)
	{
		Print(OutstandingMessages[1][1], OutstandingMessages[1][2])
		OutstandingMessages.RemoveAt(1)
		LastButton := -1
	}
return

ControllerCheck:
	if state := XInput_GetState(0) {
		wButtons := state.wButtons
		
		if(IsButtonPressed(wButtons, XINPUT_GAMEPAD_DPAD_UP))
		{
			PressedButton(1)
		}
		
		if(IsButtonPressed(wButtons, XINPUT_GAMEPAD_DPAD_RIGHT))
		{
			PressedButton(2)
		}
		
		if(IsButtonPressed(wButtons, XINPUT_GAMEPAD_DPAD_DOWN))
		{
			PressedButton(3)
		}
		
		if(IsButtonPressed(wButtons, XINPUT_GAMEPAD_DPAD_LEFT))
		{
			PressedButton(4)
		}
		
		; if(IsButtonPressed(wButtons, XINPUT_GAMEPAD_RIGHT_THUMB))
		; {
		; 	Send, {ScrollLock}
		; }
		
		if(LanguagesMenuOpen and IsButtonPressed(wButtons, XINPUT_GAMEPAD_A))
		{
			SelectLanguage()
		}
		
		if(IsButtonPressed(wButtons, XINPUT_GAMEPAD_BACK))
		{
			if(LanguagesMenuOpen)
			{
				CycleChatMode()
			}
			else {
				TimeDifference := A_TickCount - LastPressedMillis
				if(TimeDifference <= ChatOpenMillis)
				{
					Loop, 2
					{
						SecondaryMessages := !SecondaryMessages
						Loop, 4
						{
							OuterIndex := A_Index

							Loop, 4
							{
								IncrementMessage(OuterIndex, A_Index)
							}
						}
					}
					RefreshMenuTimeout()
				}
				PreviewCycle += 1
			}
		}
		
		AreBothButtonsPressed := AreBothButtonsPressed(wButtons, XINPUT_GAMEPAD_LEFT_SHOULDER, XINPUT_GAMEPAD_RIGHT_THUMB)
		IsButtonPressed(wButtons, XINPUT_GAMEPAD_B)
		
		if(ButtonsPressed[XINPUT_GAMEPAD_B] && AreBothButtonsPressed)
		{
			if(cancelMillis < A_TickCount - 1000)
			{
				cancelMillis := A_TickCount
			}
			else
			{
				ExitRocketLeague()
			}
		}
		
		if(SecondaryMessages and AreBothButtonsPressed)
		{
			ShowGui("LanguagesGui", , , , GuiHeight + 165)
			
			LanguagesMenuOpen := True
		}
		else if(AreBothButtonsPressed)
		{
			CycleChatMode()
		}
		
		if(ButtonsPressed[XINPUT_GAMEPAD_B])
		{
			TimeDifference := A_TickCount - LastPressedMillis
			if(TimeDifference <= ChatOpenMillis)
			{
				LastPressedMillis := -1
			}
		}
		
		if(!SecondaryMessages and state.sThumbRY > 15000)
		{
			RefreshMenuTimeout()
			SecondaryMessages := True
		}
		else if(SecondaryMessages and state.sThumbRY < 15000)
		{
			RefreshMenuTimeout()
			SecondaryMessages := False
		}
		
		if(!EndGameMessages and state.sThumbRY < -15000)
		{
			RefreshMenuTimeout()
			EndGameMessages := True
		}
		else if(EndGameMessages and state.sThumbRY > -15000)
		{
			RefreshMenuTimeout()
			EndGameMessages := False
		}
	}
	
	TimeDifference := A_TickCount - LastPressedMillis
	if(LanguagesMenuOpen or TimeDifference > ChatOpenMillis)
	{
		Gui, Chat: Hide
		LastButton := -1
	}
	else
	{
		UpdateTextIfDifferent("Up", 1)
		UpdateTextIfDifferent("Right", 2)
		UpdateTextIfDifferent("Left", 4)
		UpdateTextIfDifferent("Down", 3)
		GuiControl, Chat:, TimeLeft, % ConvertToSeconds(ChatOpenMillis - TimeDifference)
	}
	
	if(LanguagesMenuOpen and lastTranslate != translate)
	{
		FlagPosition := GetFlagPos(languages[translate])
		GuiControl, LanguagesGui:MoveDraw, FlagSelection, % "x" . (FlagPosition[1] - 2) . " y" . (FlagPosition[2] - 2) . " w94 h49"
		lastTranslate := translate . ""
	}
	
	rlActive := IsRocketLeagueActive()
	if(rlActive)
	{
		if(!chatModeExist)
			ShowGui("ChatMode", 5, 5, 45, 45)
		if(ChatModeSetting != PreviousChatMode)
		{
			GuiControl, ChatMode:, ChatModeIcon, % "Assets/" . GetChatModeName(ChatModeSetting) . "Chat.png"
		}
		PreviousChatMode := ChatModeSetting
		chatModeExist := True
	}
	else if(chatModeExist)  {
		Gui, ChatMode: Hide
		chatModeExist := False
	}
return

CycleChatMode()
{
	ChatModeSetting := Mod(ChatModeSetting + 1, 3)
}

SendTranslatedMessage()
{
	global
	
	InputBox, messageToTranslate, % "Send a translated message", % "Write a message in English to translate to: " . translate,,, 140
	
	if ErrorLevel
	{
		return
	}
	
	translatedMessage := GoogleTranslate(messageToTranslate, "en", translate)
	
	SendChatMessage(translatedMessage)
}

SendChatMessage(message)
{
	global
	
	message := StrReplace(message, "%20", " ")
	message := StrReplace(message, "%2C", ",")
	
	Print(message, 0)
}

GetChatModeName(chatMode)
{
	if(chatMode = 0)
		return "Team"
	if(chatMode = 1)
		return "Public"
	return "TeamOnly"
}

SetupLanguageGui()
{
	global
	Loop, Files, Assets/Flags/*.png
	{
		FlagPos := GetFlagPos(A_Index)
		Gui, LanguagesGui: Add, Picture, % "x" . FlagPos[1]-1 . " y" . FlagPos[2]-1 . " w" . (90 + 2) . " h" . (45 + 2), % "Assets/Flag Border.png"
	}
	Gui, LanguagesGui: Add, Picture, x16 y200 w94 h49 vFlagSelection, Assets/Flag Selection.png
	Loop, Files, Assets/Flags/*.png
	{
		FlagPos := GetFlagPos(A_Index)
		FlagName := SubStr(A_LoopFileName, 1, 2)
		FlagDescription := SubStr(A_LoopFileName, 4, StrLen(A_LoopFileName) - 7)
		languages[FlagName] := A_Index
		languageCount := A_Index
		Gui, LanguagesGui: Add, Picture, % "x" . FlagPos[1] . " y" . FlagPos[2] . " w90 h45 v" . CapitaliseFirstLetter(FlagName), % A_LoopFileFullPath
		Gui, LanguagesGui: Font, cFFFFFF
		Gui, LanguagesGui: Add, Text, % "x" . FlagPos[1] . " y" . (FlagPos[2] + 48), % FlagDescription
	}
}

CapitaliseFirstLetter(word)
{
	return ToUpperCase(word[1]) . SubStr(word[2], 1)
}

GetFlagPos(number)
{
	return [(30 + Mod(number - 1, 3) * 120), (90 + Floor((number - 1) / 3) * 75)]
}

ToUpperCase(string)
{
	StringUpper, upperCase, string
	return upperCase
}

InitialiseChat(allLanguages := false)
{
	for chatDirectionId, chatDirections in ObjFullyClone(jsonText)
	{
		for optionDirectionId, optionDirections in chatDirections
		{
			for phraseGroupId, phraseGroups in optionDirections
			{
				jsonText[chatDirectionId, optionDirectionId, (phraseGroupId . "Usages")] := Rand(1, jsonText[chatDirectionId, optionDirectionId, phraseGroupId].Length())
				
				Loop, Files, Assets/Flags/*.png
				{
					language := SubStr(A_LoopFileName, 1, 2)
					if((allLanguages or language == translate) and language != "en")
					{
						for phraseId, phrase in phraseGroups
						{
							if(translations[phrase] = "")
								translations[phrase] := []
							if(translations[phrase][language] = "")
								translationsModified := True
							translatedMessage := TranslateMessage(phrase, language)
							translations[phrase][language] := translatedMessage
						}
					}
				}
			}
		}
	}
	if(translationsModified)
		SaveTranslations()
}

UpdateTextIfDifferent(TextFieldName, number)
{
	GuiControlGet, currentText, Chat:, %TextFieldName%
	newMessage := GetMessage(LastButton, number, false)
	if(currentText != newMessage)
	{
		GuiControl, Chat:,% TextFieldName,% newMessage
	}
}

ConvertToSeconds(millis)
{
	Seconds := Floor(millis/1000)
	MicroSeconds := Mod(millis, 1000)
	return seconds . "." . Format("{:03}", MicroSeconds) . "s"
}

RefreshMenuTimeout()
{
	if(LastButton != -1)
		LastPressedMillis := A_TickCount
}

IsButtonPressed(wButtons, buttonNumber)
{
	if(!ButtonsPressed[buttonNumber] and wButtons & buttonNumber > 0)
	{
		ButtonsPressed[buttonNumber] := True
		return true
	}
	else if(ButtonsPressed[buttonNumber] and wButtons & buttonNumber = 0)
	{
		ButtonsPressed[buttonNumber] := False
	}
	return false
}

AreBothButtonsPressed(wButtons, buttonNumber1, buttonNumber2)
{
	return (IsButtonPressed(wButtons, buttonNumber1) and ButtonsPressed[buttonNumber2]) or (IsButtonPressed(wButtons, buttonNumber2) and ButtonsPressed[buttonNumber1])
}

#F2::
	Reload
return

#F5::
	id := WinExist("A")
	WinOffset := 1300
	WinMove, ahk_id %id%, , -WinOffset, 0, 2560+WinOffset*2, 1440
return

#F4::
	ExitRocketLeague()
return

#F3::
	Run, %A_WorkingDir%/Assets/ChatOptions.json
return

PressedButton(number)
{
	if(LanguagesMenuOpen)
	{
		if(number = 1)
			translate := RelativeFlag(-3)
		else if (number = 2)
			translate := RelativeFlag(1)
		else if (number = 3)
			translate := RelativeFlag(3)
		else if (number = 4)
			translate := RelativeFlag(-1)
		return
	}
	
	println("preview cycle: " PreviewCycle)
	
	if(!IsRocketLeagueActive())
		return
	
	TimeDifference := A_TickCount - LastPressedMillis
	if(TimeDifference < ChatOpenMillis or EndGameMessages)
	{
		if(EndGameMessages)
		{
			ScheduleMessage(GetMessage(5, number))
		}
		else if (number > 0 and LastButton > 0)
		{
			ScheduleMessage(GetMessage(LastButton, number))
		}
		LastPressedMillis := 0
		LastButton := -1
	}
	else
	{
		ShowGui("Chat")
		LastPressedMillis := A_TickCount
		LastButton := number
	}
}

PressedButtonWithoutModifier(number)
{
	global
	SetForceNoMessageControl(True)
	RefreshMenuTimeout()
	PressedButton(number)
}

ResetPreviewCycles(buttonPressed)
{
	Loop, 2
	{
		SecondaryMessages := !SecondaryMessages
		Loop, 4
		{
			OuterIndex := A_Index

			Loop, 4
			{
				if(OuterIndex != LastButton and A_Index != buttonPressed)
					IncrementMessage(OuterIndex, A_Index, -1 * PreviewCycle)
			}
		}
	}
	PreviewCycle := 0
}

RelativeFlag(relativeIndex)
{
	currentIndex := languages[translate] + 0
	currentIndex += relativeIndex
	
	while(currentIndex < 1)
		currentIndex += languageCount
	currentIndex := Mod(currentIndex - 1, languageCount) + 1
	
	for language, index in languages
	{
		if(index = currentIndex)
			return language
	}
	return ""
}

SelectLanguage()
{
	Gui, LanguagesGui: Hide
	LanguagesMenuOpen := False
	InitialiseChat()
}

ShowGui(GuiName, x = "", y = "", width = "", height = "")
{
	if(x = "")
		x := 90
	if(y = "")
		y := (A_ScreenHeight / 2) - (GuiHeight / 2)
	if(width = "")
		width := GuiWidth
	if(height = "")
		height := GuiHeight
	
	Gui %GuiName%: +LastFound +AlwaysOnTop -Caption +ToolWindow
	WinSet, Transparent, 200
	Winset, ExStyle, +0x20
	Gui, %GuiName%: Show, x%x% y%y% w%width% h%height% NoActivate, % GetFullGuiName(GuiName)
	Gui, %GuiName%: Color, 000000
}

GetFullGuiName(GuiName)
{
	return "Rocket League - " . GuiName . " Options"
}

ScheduleMessage(message)
{
	OutstandingMessages.Push([message, LastButton])
}

Print(message, lastButtonPress)
{
	global
	
	if(IsRocketLeagueActive())
	{
		MessageArray := StrSplit(message, "{break}")
		println("message is: " . message)
		Loop % MessageArray.Length()
		{
			if(GetForceNoMessageControl())
			{
				if(ChatModeSetting = 2 or ( lastButtonPress == 1 and LastButton != 1 and ChatModeSetting != 1))
				{
					Send {y Down}
					Sleep(LowCharSpeed, HighCharSpeed)
					Send {y Up}
				}
				else
				{
					Send {t Down}
					Sleep(LowCharSpeed, HighCharSpeed)
					Send {t Up}
				}
			}
			
			MessageParts := SplitMessage(MessageArray[A_Index])
			
			For i, MessagePart in MessageParts
			{
				Sleep(LowCharSpeed * 5, HighCharSpeed * 5)
				SendRaw, % MessagePart
			}
			
			SendInput {Enter down}
			Sleep(LowCharSpeed, HighCharSpeed)
			SendInput {Enter up}
			
			if (GetForceNoMessageControl())
			{
				SetForceNoMessageControl(False)
			}
			
			if(MessageArray.Length() > 1 and A_Index < MessageArray.Length())
			{
				println("sleeping!")
				Sleep(4000, 5000)
			}
		}
	}
}

IsRocketLeagueActive()
{
	WinGetTitle, WinTitle, A
	return InStr(WinTitle, "Rocket League")
}

GetMessage(arrayNumber, number, updateAfter := true)
{
	usages := 0
	secondaryTag := "Regular"
	ChatDirection := GetDirection(arrayNumber) . "Chat"
	OptionDirection := GetDirection(number)
	chatOptionsSet := jsonText[ChatDirection][OptionDirection]
	
	if(SecondaryMessages and chatOptionsSet["Extra"].Length() > 0)
	{
		secondaryTag := "Extra"
	}
	
	usages := chatOptionsSet[secondaryTag . "Usages"]
	
	usages := Mod(usages - 1, chatOptionsSet[secondaryTag].Length()) + 1
	chatOptionsSet[secondaryTag . "Usages"] := usages
	
	message := chatOptionsSet[secondaryTag][usages]
	translatedMessage := ""
	
	if(translate != "en")
	{			
		translatedMessage := TranslateMessage(message)
	}
	
	if(updateAfter)
		IncrementMessage(arrayNumber, number)
	
	return translatedMessage = "" ? message : translatedMessage
}

TranslateMessage(message, translateLanguageTo := "")
{
	if(translateLanguageTo = "")
		translateLanguageTo := translate
	
	if(translations[message][translateLanguageTo] != "")
		return translations[message][translateLanguageTo]
	
	translatedMessage := GoogleTranslate(message, "en", translateLanguageTo)
	translatedMessage := StrSplit(translatedMessage, "`n")[1]			; fix an issue where multiple phrases were being returned on multiple lines
	translations[message][translateLanguageTo] := translatedMessage
	
	Sleep, 250
	println("Translated " . message . " (en) to " . translatedMessage . " (" . translateLanguageTo . ")")
	return translatedMessage
}

SaveTranslations()
{
	if FileExist(translatedOptionsFileName)
		FileDelete, % translatedOptionsFileName
	FileAppend, % Jxon_Dump(translations), % translatedOptionsFileName
	println("Saved translations json file.")
}

IncrementMessage(arrayNumber, number, amount := 1, mode := "")
{
	println("increment message: " amount)
	ChatDirection := GetDirection(arrayNumber) . "Chat"
	OptionDirection := GetDirection(number)
	chatOptionsSet := jsonText[ChatDirection][OptionDirection]
	
	secondaryTag := "Regular"
	if(SecondaryMessages and chatOptionsSet["Extra"].Length() > 0)
	{
		secondaryTag := "Extra"
	}
	
	chatOptionsSet[secondaryTag . "Usages"] += amount
	
	while(chatOptionsSet[secondaryTag . "Usages"] < 1)
	{
		Println(chatOptionsSet[secondaryTag . "Usages"].Length())
		chatOptionsSet[secondaryTag . "Usages"] += chatOptionsSet[secondaryTag].Length()
	}
}

SetForceNoMessageControl(isEnabled)
{
	global
	
	if (isEnabled)
	{
		ForceNoMessageControlMillis := A_TickCount
	}
	else {
		ForceNoMessageControlMillis := -1
	}
}

GetForceNoMessageControl()
{
	global
	
	return ForceNoMessageControlMillis + 10000 < A_TickCount
}

SplitMessage(message)
{
	Index := 0
	MessageLength := StrLen(message)
	MessageParts := []
	While (Index < MessageLength)
	{
		MessageParts.Push(SubStr(message, Index + 1, 30))
		Index := Index + 30
	}
	return MessageParts
}

GetDirection(number)
{
	if(number = 1)
		return "Up"
	if (number = 2)
		return "Right"
	if (number = 3)
		return "Down"
	if (number = 4)
		return "Left"
	return "End"
}

Sleep(x1, x2)
{
  Random rand, x1, x2
  Sleep rand
}

Rand(x1, x2)
{
  Random rand, x1, x2
  return rand
}

RRand(x1, x2)
{
  Random rand, x1, x1+x2
  return rand
}

ExitRocketLeague()
{
	RunWait, taskkill /im RocketLeague.exe /f
}

IsArray(obj) {
	return	!!obj.MaxIndex()
}

ObjFullyClone(obj)
{
	nobj := obj.Clone()
	for k,v in nobj
		if IsObject(v)
			nobj[k] := A_ThisFunc.(v)
	return nobj
}

Println(message)
{
	FileAppend, % message . "`n", *
}

SetLanguage(language)
{
	global
	
	translate := language
	InitialiseChat()
}