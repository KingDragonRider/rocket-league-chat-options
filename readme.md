# Rocket League Chat Options

This is just a personal project to add some extra in-game chat options in Rocket League. As it's not publically available, I have not put much time into improving the usability of this tool... That will happen soon! It therefore currently only works with an Xbox Controller and on Windows 10.

#### To set up the tool:

1. Download "AutoHotKey" ([link](https://autohotkey.com), a scripting language processor for Windows. (which this project is written in)
2. Download this repository ([link](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/get/master.zip) and extract the folder somewhere accessible on your computer.
3. Configure your rocket league game to work with the plugin:
    1. Open the in-game options menu in rocket league
    2. In the "Controls" settings page, unassign all quick chat buttons for the Xbox controller
    3. In the "Video" settings page, change the display mode to "Borderless" (To allow the chat overlay to be drawn above the game)
4. Navigate to the folder you extracted above (Step 2) and run the "Rocket League - Extra Chat Options.ahk" file (which should have an H icon. If it doesn't, AutoHotKey hasn't been installed correctly on your computer)
5. If it runs, you will see an icon that looks like the image underneath this list
    1. Right-clicking this will give you the options to stop the script running ("Exit") or to reload it ("Reload This Script"). You will need to reload the plugin every time you change the ChatOptions.json (discussed more later)

![App Icon](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/downloads/2b37YSPjpd.png)

#### Using the tool in-game:

- To access the chat menu, use one of the D Pad buttons (the controller arrow keys). Bear in mind that the plugin _does not_ detect if you're in a game when you press the button, so you cannot use these arrow keys to navigate the menus any more. Instead, you will have to use the left Thumb Stick on your controller.
- To cycle through the options, press the back button on your controller (The button that looks like two intersecting rectangles) while you have the chat options open. There is a timer at the top right showing when the chat menu is going to disappear.
- There is an "Extra" set of chat options if you move the _Right_ thumb stick _up_.
- For "End Game" chat options ("gg", "Thanks for the game", etc.) move the _Right_ thumb stick _down_ and use any of the D-Pad buttons (You only need to press them once)
- You will also notice that chat bubbles appear on the top left. These indicate which chat channel you're speaking in, as shown below. You can cycle through these by pressing both shoulder buttons on your xbox controller at once.
    - ![Public Chat](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/raw/d22a4f83423743194968eb978d646cd041d02c58/Assets/TeamChat.png) - **Default Chat**
    - ![Public Chat](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/raw/d22a4f83423743194968eb978d646cd041d02c58/Assets/PublicChat.png) - **Public Only Chat**
    - ![Public Chat](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/raw/d22a4f83423743194968eb978d646cd041d02c58/Assets/TeamOnlyChat.png) - **Team Only Chat**
    
#### Changing the chat options

To change the chat options, navigate to the folder you extracted previously, open the "Assets" folder and finally the "ChatOptions.json" file. You might want to use an editor such as Notepad++ to view this file easier.

The file contains each set of phrases for each type of key combination. This is a JSON file, and as such you must ensure that the format of the file is correct otherwise the program will not run. https://jsonformatter.curiousconcept.com/ is a good tool where you can copy and paste all the text in the ChatOptions.json file and it will tell you if you have formatted the file correctly. If you have any trouble with it, send the file over to me and I'll take a look for you :)

Finally, to apply the new changes, right-click the icon at the bottom right of your computer, right-click it and press "Reload This Script". It might take a couple moments to translate the new changes (about 2 seconds for each new phrase), but then the app will start working again once it has completed.

To change the language, push the _right_ thumb stick _up_ and then press both shoulder buttons at the same time (in that order). Select the language you want with the D Pad buttons (the one with an orange outline) and then press the (A) button on your controller.
    I definitely need to improve this... but there's not that many buttons on an xbox controller! If you want it changed to something else I can do that for you though :)

### The chat overlay:
![Chat Overlay](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/downloads/RocketLeague_GQkKkQ8mej.png)

### Language selection:
![Chat Languages](https://bitbucket.org/KingDragonRider/rocket-league-chat-options/downloads/AutoHotkey_b6bJfLILrp.png)